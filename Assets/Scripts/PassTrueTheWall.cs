﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassTrueTheWall : MonoBehaviour
{

    public GameObject coin, wall;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            
            Destroy(coin);
            Destroy(wall);
        }
    }

}
