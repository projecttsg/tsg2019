﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn_OnRocketHit : MonoBehaviour
{
    public GameObject Player;
    public GameObject PlayerCam;


    public Vector3 startposition, camstartposition;


    void Start()
    {
        PlayerCam.transform.parent = Player.transform;
    }

    IEnumerator OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerCam.transform.parent = null;
            Player.transform.position = new Vector3(22f, 22f, 22f);
            yield return new WaitForSeconds(1f);           
            Player.transform.position = startposition;
            PlayerCam.transform.position = camstartposition;
            PlayerCam.transform.parent = Player.transform;
        }


    }
}
