﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchLevelScript : MonoBehaviour
{
    public GameObject level, ground, disable, grounddisable, player;
    public Vector3 startposition;

    void Start()
    {
        
    }

     void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            level.SetActive(true);
            ground.SetActive(true);
            disable.SetActive(false);
            grounddisable.SetActive(false);

            player.transform.position = startposition;
        }


    }

}
