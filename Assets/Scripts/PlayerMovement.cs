﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float movespeed;
    public Rigidbody rb;
    public Vector3 movement;

    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        movement = new Vector3(Input.GetAxis("Vertical"), 0f, Input.GetAxis("Horizontal"));
    }
    void FixedUpdate()
    {
        moveplayer(movement);
    }

    void moveplayer(Vector3 direction)
    {
        rb.MovePosition(transform.position + (direction * movespeed * Time.deltaTime));
        //rb.velocity = direction * movespeed;
    }
                        
}
